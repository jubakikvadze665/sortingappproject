package org.example;

/**
 * Hello world!
 *
 */
public class App 
{

    public static String SortForMe(String [] args){
        if (args == null)
        {
            throw new IllegalArgumentException();
        }

        int [] array = new int[args.length];


        for(int i=0; i<args.length; i++){
            try {
                array[i] = Integer.parseInt(args[i]);
            }catch (Exception e){
                throw new IllegalArgumentException("Not valid!", e);
            }
        }

        for (int i=0; i<array.length; i++)
        {
            int val = array[i];
            int j = i-1;

            while (j >= 0 && val < array[j])
            {
                array[j+1] = array[j];
                j--;
            }
            array[j+1] = val;
        }

        String ans = "";
        for(int i=0; i<array.length; i++){
            if (i == array.length-1){
                ans = ans + array[i];
            }else{
               ans = ans + array[i] + " ";
            }
        }
        return ans;
    }


    public static void main( String[] args )
    {
        SortForMe(args);
    }
}
