package org.example;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.
public class AppTestTest {
    App app = new App();


    @Test
    public void Checker(){
        String s = app.SortForMe(new String[]{"4", "2", "1", "5", "3"});
       assertEquals("1 2 3 4 5", s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullChecker(){
        String s = app.SortForMe(null);
    }


    @Test
    public void singleElementChecker(){
        String s = app.SortForMe(new String []  {"1"});
        assertEquals("1", s);
    }


    @Test
    public void zeroElementChecker(){
        String s = app.SortForMe(new String [] {});
        assertEquals("", s);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonIntegerTypeChecker(){
        String s = app.SortForMe(new String [] {"1", "Juba"});
    }

}
