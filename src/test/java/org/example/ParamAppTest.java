package org.example;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParamAppTest {


    App app = new App();

    private String [] array;
    private String expected;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                  {new String []{"3","4","1"}, "1 3 4"},
                  {new String []{"4","4","1", "2"}, "1 2 4 4"},
                  {new String [] {"70", "45", "23"}, "23 45 70"}
        });
    }
    public ParamAppTest (String [] array, String expected){
        this.array = array;
        this.expected = expected;
    }

    @Test
    public void ParamChecker(){
        String s = app.SortForMe(array);
        assertEquals(expected, s);
    }

}
